import java.util.Scanner;

class Main {

    public int[][] verifyVisit(int c, int r, int[][] grid){
        boolean isVisit = (grid[c][r] == 1);

        if ( isVisit ){
            grid[c][r] = 0;
        } else {
            grid[c][r] = 1;
        }

        return grid;
    }

    public void printDirect(int c, int r, String direct, int[][] grid){
        boolean visit = (grid[c][r] == 1);

        if (visit){
            System.out.println("("+c+","+r+") =" + direct);
        }
    }
    /**
     * this method define the road to go
     * @grid is array bidimensional, size defined by lenght
     * @lenght is demension for grid
     */
    public void each(int[][] grid, int lenght){
        int[][] gridEach = grid;
        int i=0, j=0, k=0, l=0, m=0;
        /** 
         *for each var i is minor or equal to lenght / 2 
        */
        for (i = 0; i <= lenght / 2; i++){
            //this foreach define to go to right
            for(j = i; j < lenght - l - 1; j++){
                //verify place if was visited
                gridEach = this.verifyVisit(i, j, grid);
                //indicate to go to right
                this.printDirect(i, j, "R", gridEach);
            }
            //this foreach define to go to down
            for (k = i; k < lenght - i - 1; k++){
                gridEach = this.verifyVisit(k, j, grid);
                this.printDirect(k, j, "D", gridEach);
            }
            //this foreach define to go to left
            for (l = k; l > lenght - k - 1; l--){
                gridEach = this.verifyVisit(k, l, grid);
                this.printDirect(k, l, "L", gridEach);
            }
            //this foreach define to go to up
            for (m = k; m > lenght - k - 1; m--){
                gridEach = this.verifyVisit(m, l, grid);
                this.printDirect(m, l, "U", gridEach);
            }
        }
    }

    public static void main(String [] args){
        Main main = new Main(); //instance this class
        Scanner scn = new Scanner(System.in); //instance scanner
        System.out.print("Ingresa la dimensión: "); //request a dimension
        int lenght = scn.nextInt(); //asigned dimension from console to var lenght

        int[][] grid = new int[lenght][lenght]; //defined grid

        main.each(grid, lenght); //for each grid 
    }
}